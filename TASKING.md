- 给定一个存包小弟，一个包（大中小迷你）和一个有容量的（大中小）储物柜，当把包给存包小弟，用户应该得到一个小票。
- 给定一个存包小弟，一个包（大中小迷你）和一个无容量的（大中小）储物柜，当把包给存包小弟，用户应该存包失败，得到提示信息。
- 给定一个存包小弟，小票和一个存有包的（大中小）储物柜，当把小票给存包小弟，用户应该可以得到自己的包。
- 给定一个存包小弟，一个大包，一个无容量的大储物柜和一个有容量的储物柜，当把包给存包小弟，用户应该得到一个小票。
- 给定一个大包、存包小弟，一个无容量的大储物柜和一个有容量的大储物柜，把大包给小弟得到小票后，当把小票给存包小弟，用户应该得到原来的包。
- 给定一个大包、存包小弟A，存包小弟B，一个无容量的大储物柜和一个有容量的大储物柜，把大包给小弟A得到小票后，当把小票给存包小弟B，用户应该得到原来的包。