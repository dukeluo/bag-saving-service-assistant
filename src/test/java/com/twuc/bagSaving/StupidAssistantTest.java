package com.twuc.bagSaving;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.locks.Lock;

import static com.twuc.bagSaving.CabinetFactory.createCabinetWithPlentyOfCapacity;
import static org.junit.jupiter.api.Assertions.*;

public class StupidAssistantTest {
    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgument#createCabinetWithOnlyOneEmptyLockerAndSavableSizes")
    void should_get_a_ticket_when_get_a_bag_to_stupid_assistant(Cabinet cabinet, BagSize bagSize, LockerSize lockerSize) {
        StupidAssistant stupidAssistant = new StupidAssistant();
        Bag bag = new Bag(bagSize);
        List<Cabinet> cabinets = Arrays.asList(cabinet);
        Ticket ticket = stupidAssistant.saveBag(bag, cabinets, lockerSize);

        assertNotNull(ticket);
    }

    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgument#createCabinetWithOnlyOneLockerSizeFull")
    void should_throw_when_get_bag_to_the_stupid_assistant_if_correspond_lockers_are_full(
            Cabinet fullCabinet, BagSize bagSize, LockerSize lockerSize) {
        StupidAssistant stupidAssistant = new StupidAssistant();
        Bag savedBag = new Bag(bagSize);
        List<Cabinet> cabinets = Arrays.asList(fullCabinet);
        InsufficientLockersException error = assertThrows(
                InsufficientLockersException.class,
                () -> stupidAssistant.saveBag(savedBag, cabinets, lockerSize));

        assertEquals("Insufficient empty lockers.", error.getMessage());
    }

    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgument#createCabinetWithOnlyOneEmptyLockerAndSavableSizes")
    void should_get_bag_when_get_a_ticket_to_stupid_assistant(Cabinet cabinet, BagSize bagSize, LockerSize lockerSize) {
        StupidAssistant stupidAssistant = new StupidAssistant();
        Bag bag = new Bag(bagSize);
        List<Cabinet> cabinets = Arrays.asList(cabinet);
        Ticket ticket = stupidAssistant.saveBag(bag, cabinets, lockerSize);
        Bag savedBag = stupidAssistant.getBag(ticket, cabinets, lockerSize);

        assertSame(bag, savedBag);
    }

    @Test
    void should_get_a_ticket_when_a_cabinet_is_full_and_another_is_not() {
        StupidAssistant stupidAssistant = new StupidAssistant();
        Bag bag = new Bag(BagSize.BIG);
        Bag bagAnother = new Bag(BagSize.BIG);
        Cabinet cabinet = new Cabinet(new LockerSetting(LockerSize.BIG, 1));
        Cabinet cabinetAnother = new Cabinet(new LockerSetting(LockerSize.BIG, 1));
        List<Cabinet> cabinets = Arrays.asList(cabinet, cabinetAnother);

        cabinet.save(bagAnother, LockerSize.BIG);
        Ticket ticket = stupidAssistant.saveBag(bag, cabinets, LockerSize.BIG);
        assertNotNull(ticket);
    }

    @Test
    void should_get_bag_when_stupid_assistant_manager_two_cabinets() {
        StupidAssistant stupidAssistant = new StupidAssistant();
        Bag bag = new Bag(BagSize.BIG);
        Bag bagAnother = new Bag(BagSize.BIG);
        Cabinet cabinet = new Cabinet(new LockerSetting(LockerSize.BIG, 1));
        Cabinet cabinetAnother = new Cabinet(new LockerSetting(LockerSize.BIG, 1));
        List<Cabinet> cabinets = Arrays.asList(cabinet, cabinetAnother);

        cabinet.save(bagAnother, LockerSize.BIG);
        Ticket ticket = stupidAssistant.saveBag(bag, cabinets, LockerSize.BIG);
        Bag savedBag = stupidAssistant.getBag(ticket, cabinets, LockerSize.BIG);

        assertSame(bag, savedBag);
    }

    @Test
    void should_get_bag_when_save_by_stupid_assistant_a_and_get_by_assistant_b() {
        StupidAssistant stupidAssistantA = new StupidAssistant();
        StupidAssistant stupidAssistantB = new StupidAssistant();

        Bag bag = new Bag(BagSize.BIG);
        Bag bagAnother = new Bag(BagSize.BIG);

        Cabinet cabinet = new Cabinet(new LockerSetting(LockerSize.BIG, 1));
        cabinet.save(bagAnother, LockerSize.BIG);

        Cabinet cabinetAnother = new Cabinet(new LockerSetting(LockerSize.BIG, 1));
        List<Cabinet> cabinets = Arrays.asList(cabinet, cabinetAnother);

        Ticket ticket = stupidAssistantA.saveBag(bag, cabinets, LockerSize.BIG);
        Bag savedBag = stupidAssistantB.getBag(ticket, cabinets, LockerSize.BIG);

        assertSame(bag, savedBag);
    }
}
