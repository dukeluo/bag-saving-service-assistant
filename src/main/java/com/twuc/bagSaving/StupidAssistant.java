package com.twuc.bagSaving;

import java.util.List;

public class StupidAssistant {

    public Ticket saveBag(Bag bag, List<Cabinet> cabinets, LockerSize lockerSize) {

        for (Cabinet cabinet: cabinets) {
            if (!cabinet.isFull(lockerSize)) {
                return cabinet.save(bag,lockerSize);
            }
        }
        throw new InsufficientLockersException("Insufficient empty lockers.");
    }

    public Bag getBag(Ticket ticket, List<Cabinet> cabinets, LockerSize lockerSize) {
        for (Cabinet cabinet : cabinets) {
            if (cabinet.hasTicket(lockerSize, ticket)) {
                return cabinet.getBag(ticket);
            }
        }
        throw new IllegalArgumentException("Invalid ticket.");
    }
}
